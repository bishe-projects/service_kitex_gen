// Code generated by Kitex v0.2.1. DO NOT EDIT.

package commodityservice

import (
	"context"
	"github.com/cloudwego/kitex/client"
	"github.com/cloudwego/kitex/client/callopt"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/demo_commodity"
)

// Client is designed to provide IDL-compatible methods with call-option parameter for kitex framework.
type Client interface {
	GetCommodityById(ctx context.Context, req *demo_commodity.GetCommodityByIdReq, callOptions ...callopt.Option) (r *demo_commodity.GetCommodityByIdResp, err error)
	GetCommodityList(ctx context.Context, req *demo_commodity.GetCommodityListReq, callOptions ...callopt.Option) (r *demo_commodity.GetCommodityListResp, err error)
	GetCommodityListByIds(ctx context.Context, req *demo_commodity.GetCommodityListByIdsReq, callOptions ...callopt.Option) (r *demo_commodity.GetCommodityListByIdsResp, err error)
	GetCommodityRankList(ctx context.Context, req *demo_commodity.GetCommodityRankListReq, callOptions ...callopt.Option) (r *demo_commodity.GetCommodityRankListResp, err error)
	HasFavoriteCommodity(ctx context.Context, req *demo_commodity.HasFavoriteCommodityReq, callOptions ...callopt.Option) (r *demo_commodity.HasFavoriteCommodityResp, err error)
	FavoriteCommodity(ctx context.Context, req *demo_commodity.FavoriteCommodityReq, callOptions ...callopt.Option) (r *demo_commodity.FavoriteCommodityResp, err error)
	FavoriteCancelCommodity(ctx context.Context, req *demo_commodity.FavoriteCancelCommodityReq, callOptions ...callopt.Option) (r *demo_commodity.FavoriteCancelCommodityResp, err error)
	GetFavoriteList(ctx context.Context, req *demo_commodity.GetFavoriteListReq, callOptions ...callopt.Option) (r *demo_commodity.GetFavoriteListResp, err error)
	GetCommodityCommentList(ctx context.Context, req *demo_commodity.GetCommodityCommentListReq, callOptions ...callopt.Option) (r *demo_commodity.GetCommodityCommentListResp, err error)
	BuyCommodity(ctx context.Context, req *demo_commodity.BuyCommodityReq, callOptions ...callopt.Option) (r *demo_commodity.BuyCommodityResp, err error)
	GetOrderList(ctx context.Context, req *demo_commodity.GetOrderListReq, callOptions ...callopt.Option) (r *demo_commodity.GetOrderListResp, err error)
	CommentOrder(ctx context.Context, req *demo_commodity.CommentOrderReq, callOptions ...callopt.Option) (r *demo_commodity.CommentOrderResp, err error)
}

// NewClient creates a client for the service defined in IDL.
func NewClient(destService string, opts ...client.Option) (Client, error) {
	var options []client.Option
	options = append(options, client.WithDestService(destService))

	options = append(options, opts...)

	kc, err := client.NewClient(serviceInfo(), options...)
	if err != nil {
		return nil, err
	}
	return &kCommodityServiceClient{
		kClient: newServiceClient(kc),
	}, nil
}

// MustNewClient creates a client for the service defined in IDL. It panics if any error occurs.
func MustNewClient(destService string, opts ...client.Option) Client {
	kc, err := NewClient(destService, opts...)
	if err != nil {
		panic(err)
	}
	return kc
}

type kCommodityServiceClient struct {
	*kClient
}

func (p *kCommodityServiceClient) GetCommodityById(ctx context.Context, req *demo_commodity.GetCommodityByIdReq, callOptions ...callopt.Option) (r *demo_commodity.GetCommodityByIdResp, err error) {
	ctx = client.NewCtxWithCallOptions(ctx, callOptions)
	return p.kClient.GetCommodityById(ctx, req)
}

func (p *kCommodityServiceClient) GetCommodityList(ctx context.Context, req *demo_commodity.GetCommodityListReq, callOptions ...callopt.Option) (r *demo_commodity.GetCommodityListResp, err error) {
	ctx = client.NewCtxWithCallOptions(ctx, callOptions)
	return p.kClient.GetCommodityList(ctx, req)
}

func (p *kCommodityServiceClient) GetCommodityListByIds(ctx context.Context, req *demo_commodity.GetCommodityListByIdsReq, callOptions ...callopt.Option) (r *demo_commodity.GetCommodityListByIdsResp, err error) {
	ctx = client.NewCtxWithCallOptions(ctx, callOptions)
	return p.kClient.GetCommodityListByIds(ctx, req)
}

func (p *kCommodityServiceClient) GetCommodityRankList(ctx context.Context, req *demo_commodity.GetCommodityRankListReq, callOptions ...callopt.Option) (r *demo_commodity.GetCommodityRankListResp, err error) {
	ctx = client.NewCtxWithCallOptions(ctx, callOptions)
	return p.kClient.GetCommodityRankList(ctx, req)
}

func (p *kCommodityServiceClient) HasFavoriteCommodity(ctx context.Context, req *demo_commodity.HasFavoriteCommodityReq, callOptions ...callopt.Option) (r *demo_commodity.HasFavoriteCommodityResp, err error) {
	ctx = client.NewCtxWithCallOptions(ctx, callOptions)
	return p.kClient.HasFavoriteCommodity(ctx, req)
}

func (p *kCommodityServiceClient) FavoriteCommodity(ctx context.Context, req *demo_commodity.FavoriteCommodityReq, callOptions ...callopt.Option) (r *demo_commodity.FavoriteCommodityResp, err error) {
	ctx = client.NewCtxWithCallOptions(ctx, callOptions)
	return p.kClient.FavoriteCommodity(ctx, req)
}

func (p *kCommodityServiceClient) FavoriteCancelCommodity(ctx context.Context, req *demo_commodity.FavoriteCancelCommodityReq, callOptions ...callopt.Option) (r *demo_commodity.FavoriteCancelCommodityResp, err error) {
	ctx = client.NewCtxWithCallOptions(ctx, callOptions)
	return p.kClient.FavoriteCancelCommodity(ctx, req)
}

func (p *kCommodityServiceClient) GetFavoriteList(ctx context.Context, req *demo_commodity.GetFavoriteListReq, callOptions ...callopt.Option) (r *demo_commodity.GetFavoriteListResp, err error) {
	ctx = client.NewCtxWithCallOptions(ctx, callOptions)
	return p.kClient.GetFavoriteList(ctx, req)
}

func (p *kCommodityServiceClient) GetCommodityCommentList(ctx context.Context, req *demo_commodity.GetCommodityCommentListReq, callOptions ...callopt.Option) (r *demo_commodity.GetCommodityCommentListResp, err error) {
	ctx = client.NewCtxWithCallOptions(ctx, callOptions)
	return p.kClient.GetCommodityCommentList(ctx, req)
}

func (p *kCommodityServiceClient) BuyCommodity(ctx context.Context, req *demo_commodity.BuyCommodityReq, callOptions ...callopt.Option) (r *demo_commodity.BuyCommodityResp, err error) {
	ctx = client.NewCtxWithCallOptions(ctx, callOptions)
	return p.kClient.BuyCommodity(ctx, req)
}

func (p *kCommodityServiceClient) GetOrderList(ctx context.Context, req *demo_commodity.GetOrderListReq, callOptions ...callopt.Option) (r *demo_commodity.GetOrderListResp, err error) {
	ctx = client.NewCtxWithCallOptions(ctx, callOptions)
	return p.kClient.GetOrderList(ctx, req)
}

func (p *kCommodityServiceClient) CommentOrder(ctx context.Context, req *demo_commodity.CommentOrderReq, callOptions ...callopt.Option) (r *demo_commodity.CommentOrderResp, err error) {
	ctx = client.NewCtxWithCallOptions(ctx, callOptions)
	return p.kClient.CommentOrder(ctx, req)
}
