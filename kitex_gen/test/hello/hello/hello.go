// Code generated by Kitex v0.0.5. DO NOT EDIT.

package hello

import (
	"bishe/service_kitex_gen/kitex_gen/test/hello"
	"context"
	"github.com/cloudwego/kitex/client"
	kitex "github.com/cloudwego/kitex/pkg/serviceinfo"
)

func serviceInfo() *kitex.ServiceInfo {
	return helloServiceInfo
}

var helloServiceInfo = newServiceInfo()

func newServiceInfo() *kitex.ServiceInfo {
	serviceName := "Hello"
	handlerType := (*hello.Hello)(nil)
	methods := map[string]kitex.MethodInfo{
		"echo": kitex.NewMethodInfo(echoHandler, newHelloEchoArgs, newHelloEchoResult, false),
		"add":  kitex.NewMethodInfo(addHandler, newHelloAddArgs, newHelloAddResult, false),
	}
	extra := map[string]interface{}{
		"PackageName": "hello",
	}
	svcInfo := &kitex.ServiceInfo{
		ServiceName:     serviceName,
		HandlerType:     handlerType,
		Methods:         methods,
		PayloadCodec:    kitex.Thrift,
		KiteXGenVersion: "v0.0.5",
		Extra:           extra,
	}
	return svcInfo
}

func echoHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*hello.HelloEchoArgs)
	realResult := result.(*hello.HelloEchoResult)
	success, err := handler.(hello.Hello).Echo(ctx, realArg.Req)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newHelloEchoArgs() interface{} {
	return hello.NewHelloEchoArgs()
}

func newHelloEchoResult() interface{} {
	return hello.NewHelloEchoResult()
}

func addHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*hello.HelloAddArgs)
	realResult := result.(*hello.HelloAddResult)
	success, err := handler.(hello.Hello).Add(ctx, realArg.Req)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newHelloAddArgs() interface{} {
	return hello.NewHelloAddArgs()
}

func newHelloAddResult() interface{} {
	return hello.NewHelloAddResult()
}

type kClient struct {
	c client.Client
}

func newServiceClient(c client.Client) *kClient {
	return &kClient{
		c: c,
	}
}

func (p *kClient) Echo(ctx context.Context, req *hello.Request) (r *hello.Response, err error) {
	var _args hello.HelloEchoArgs
	_args.Req = req
	var _result hello.HelloEchoResult
	if err = p.c.Call(ctx, "echo", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}

func (p *kClient) Add(ctx context.Context, req *hello.AddRequest) (r *hello.AddResponse, err error) {
	var _args hello.HelloAddArgs
	_args.Req = req
	var _result hello.HelloAddResult
	if err = p.c.Call(ctx, "add", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}
