namespace go demo_commodity
include '../base/base.thrift'
include '../comment/comment.thrift'

struct Commodity {
  1: i64 id,
  2: string name,
  3: string desc,
  4: double price
}

struct Order {
  1: i64 id,
  2: Commodity commodity,
  3: i64 uid,
  4: i64 amount,
  5: string createTime,
  6: i32 status
}

struct GetCommodityByIdReq {
  1: required i64 commodityId,
  255: optional base.Base Base
}

struct GetCommodityByIdResp {
  1: optional Commodity commodity,
  255: optional base.BaseResp BaseResp
}

struct GetCommodityListReq {
  255: optional base.Base Base
}

struct GetCommodityListResp {
  1: required list<Commodity> commodityList,
  255: optional base.BaseResp BaseResp
}

struct GetCommodityListByIdsReq {
  1: required list<i64> ids,
  255: optional base.Base Base
}

struct GetCommodityListByIdsResp {
  1: required list<Commodity> commodityList,
  255: optional base.BaseResp BaseResp
}

struct GetCommodityRankListReq {
  255: optional base.Base Base
}

struct GetCommodityRankListResp {
  1: required list<Commodity> rankList,
  255: optional base.BaseResp BaseResp
}

struct HasFavoriteCommodityReq {
  1: required i64 commodityId,
  255: optional base.Base Base
}

struct HasFavoriteCommodityResp {
  1: required bool has,
  255: optional base.BaseResp BaseResp
}

struct FavoriteCommodityReq {
  1: required i64 commodityId,
  255: optional base.Base Base
}

struct FavoriteCommodityResp {
  255: optional base.BaseResp BaseResp
}

struct FavoriteCancelCommodityReq {
  1: required i64 commodityId,
  255: optional base.Base Base
}

struct FavoriteCancelCommodityResp {
  255: optional base.BaseResp BaseResp
}

struct GetFavoriteListReq {
  255: optional base.Base Base
}

struct GetFavoriteListResp {
  1: required list<Commodity> favoriteList,
  255: optional base.BaseResp BaseResp
}

struct User {
  1: i64 uid,
  2: string username
}

struct UserComment {
  1: comment.Comment comment,
  2: User user
}

struct GetCommodityCommentListReq {
  1: required i64 commodityId,
  255: optional base.Base Base
}

struct GetCommodityCommentListResp {
  1: required list<UserComment> commentList,
  255: optional base.BaseResp BaseResp
}

struct BuyCommodityReq {
  1: required i64 commodityId,
  2: required i64 amount,
  255: optional base.Base Base
}

struct BuyCommodityResp {
  255: optional base.BaseResp BaseResp
}

struct GetOrderListReq {
  255: optional base.Base Base
}

struct GetOrderListResp {
  1: required list<Order> orderList,
  255: optional base.BaseResp BaseResp
}

struct CommentOrderReq {
  1: required i64 orderId,
  2: required string content,
  255: optional base.Base Base
}

struct CommentOrderResp {
  255: optional base.BaseResp BaseResp
}

service CommodityService {
  GetCommodityByIdResp getCommodityById(1: GetCommodityByIdReq req)
  GetCommodityListResp getCommodityList(1: GetCommodityListReq req)
  GetCommodityListByIdsResp getCommodityListByIds(1: GetCommodityListByIdsReq req)
  GetCommodityRankListResp getCommodityRankList(1: GetCommodityRankListReq req)
  HasFavoriteCommodityResp hasFavoriteCommodity(1: HasFavoriteCommodityReq req)
  FavoriteCommodityResp favoriteCommodity(1: FavoriteCommodityReq req)
  FavoriteCancelCommodityResp favoriteCancelCommodity(1: FavoriteCancelCommodityReq req)
  GetFavoriteListResp getFavoriteList(1: GetFavoriteListReq req)
  GetCommodityCommentListResp getCommodityCommentList(1: GetCommodityCommentListReq req)
  BuyCommodityResp buyCommodity(1: BuyCommodityReq req)
  GetOrderListResp getOrderList(1: GetOrderListReq req)
  CommentOrderResp commentOrder(1: CommentOrderReq req)
}