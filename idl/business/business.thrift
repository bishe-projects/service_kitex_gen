namespace go business
include '../base/base.thrift'

struct Business {
  1: i64 id,
  2: string name,
  3: string desc
}

struct CreateBusinessReq {
  1: required string name,
  2: required string desc,
  255: optional base.Base Base
}

struct CreateBusinessResp {
  1: optional Business business,
  255: optional base.BaseResp BaseResp
}

struct GetBusinessByIDReq {
  1: required i64 businessId,
  255: optional base.Base Base
}

struct GetBusinessByIDResp {
  1: optional Business business,
  255: optional base.BaseResp BaseResp
}

struct GetBusinessByNameReq {
  1: required string businessName,
  255: optional base.Base Base
}

struct GetBusinessByNameResp {
  1: optional Business business,
  255: optional base.BaseResp BaseResp
}

struct AllBusinessListReq {
  255: optional base.Base Base
}

struct AllBusinessListResp {
  1: required list<Business> businessList,
  255: optional base.BaseResp BaseResp
}

service BusinessService {
  CreateBusinessResp createBusiness(1: CreateBusinessReq req)
  GetBusinessByIDResp getBusinessByID(1: GetBusinessByIDReq req)
  GetBusinessByNameResp getBusinessByName(1: GetBusinessByNameReq req)
  AllBusinessListResp allBusinessList(1: AllBusinessListReq req)
}