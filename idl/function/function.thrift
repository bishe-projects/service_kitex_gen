namespace go function
include '../base/base.thrift'

struct Function {
	1: i64 id,
	2: string name,
	3: string desc,
	4: string path,
	5: string icon
}

struct GetFunctionByIDReq {
	1: required i64 functionId,
	255: optional base.Base Base
}

struct GetFunctionByIDResp {
	1: optional Function function,
	255: optional base.BaseResp BaseResp
}

struct AllFunctionListReq {
	255: optional base.Base Base
}

struct AllFunctionListResp {
	1: required list<Function> functionList,
	255: optional base.BaseResp BaseResp
}

service FunctionService {
	GetFunctionByIDResp getFunctionByID(1: GetFunctionByIDReq req)
	AllFunctionListResp allFunctionList(1: AllFunctionListReq req)
}