namespace go base

struct Base {
    1: string caller,
    2: string token,
    255: map<string, string> extra
}

struct BaseResp {
    1: i64 status,
    2: string message
}