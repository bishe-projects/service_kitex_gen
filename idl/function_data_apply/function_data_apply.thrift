namespace go function_data_apply
include '../base/base.thrift'

struct FunctionDataApply {
  1: i64 id,
  2: i64 applicantId,
  3: string applicantName,
  4: i64 functionId,
  5: string functionName,
  6: i64 businessId,
  7: string businessName,
  8: i32 status
}

struct GetApplyListReq {
  1: optional i64 applicantId,
  2: optional i64 functionId,
  3: optional i64 businessId,
  4: optional i32 status,
  255: optional base.Base Base
}

struct GetApplyListResp {
  1: required list<FunctionDataApply> applyList,
  255: optional base.BaseResp BaseResp
}

struct CreateApplyReq {
  1: required i64 functionId,
  2: required i64 businessId,
  255: optional base.Base Base
}

struct CreateApplyResp {
  255: optional base.BaseResp BaseResp
}

struct ApproveApplyReq {
  1: required i64 applyId,
  255: optional base.Base Base
}

struct ApproveApplyResp {
  255: optional base.BaseResp BaseResp
}

struct RejectApplyReq {
  1: required i64 applyId,
  255: optional base.Base Base
}

struct RejectApplyResp {
  255: optional base.BaseResp BaseResp
}

struct WithdrawApplyReq {
  1: required i64 applyId,
  255: optional base.Base Base
}

struct WithdrawApplyResp {
  255: optional base.BaseResp BaseResp
}

struct AdminApplyListReq {
  1: required i64 functionId,
  2: optional i32 status,
  255: optional base.Base Base
}

struct AdminApplyListResp {
  1: required list<FunctionDataApply> applyList,
  255: optional base.BaseResp BaseResp
}

struct OwnApplyListReq {
  1: required i64 functionId,
  2: optional i32 status,
  255: optional base.Base Base
}

struct OwnApplyListResp {
  1: required list<FunctionDataApply> applyList,
  255: optional base.BaseResp BaseResp
}

service FunctionDataApplyService {
  GetApplyListResp getApplyList(1: GetApplyListReq req)
  CreateApplyResp createApply(1: CreateApplyReq req)
  ApproveApplyResp approveApply(1: ApproveApplyReq req)
  RejectApplyResp rejectApply(1: RejectApplyReq req)
  WithdrawApplyResp withdrawApply(1: WithdrawApplyReq req)
  AdminApplyListResp adminApplyList(1: AdminApplyListReq req)
  OwnApplyListResp ownApplyList(1: OwnApplyListReq req)
}