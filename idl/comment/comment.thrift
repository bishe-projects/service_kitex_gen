namespace go comment
include '../base/base.thrift'

struct CommentEntity {
  1: i64 id,
  2: i64 bizId,
  3: string name,
  4: string desc
}

struct Comment {
  1: i64 id,
  2: i64 bizId,
  3: i64 entityTypeId,
  4: i64 entityId,
  5: i64 uid,
  6: i64 parentId,
  7: string content,
  8: string createTime,
  9: i64 replyCount
}

struct CreateCommentEntityReq {
  1: required i64 bizId,
  2: required string name,
  3: required string desc,
  255: optional base.Base Base
}

struct CreateCommentEntityResp {
  1: optional CommentEntity commentEntity,
  255: optional base.BaseResp BaseResp
}

struct AllCommentEntityListReq {
  1: optional i64 bizId,
  255: optional base.Base Base
}

struct AllCommentEntityListResp {
  1: required list<CommentEntity> commentEntityList,
  255: optional base.BaseResp BaseResp
}

struct PublishCommentReq {
  1: required i64 bizId,
  2: required i64 entityTypeId,
  3: required i64 entityId,
  4: required i64 uid,
  5: required string content,
  6: required i64 parentId,
  255: optional base.Base Base
}

struct PublishCommentResp {
  1: optional Comment comment,
  255: optional base.BaseResp BaseResp
}

struct DeleteCommentReq {
  1: required i64 id,
  2: required i64 bizId,
  255: optional base.Base Base
}

struct DeleteCommentResp {
  255: optional base.BaseResp BaseResp
}

struct GetCommentByIdReq {
  1: required i64 id,
  2: required i64 bizId,
  255: optional base.Base Base
}

struct GetCommentByIdResp {
  1: optional Comment comment,
  255: optional base.BaseResp BaseResp
}

struct EntityCommentListReq {
  1: required i64 bizId,
  2: required i64 entityTypeId,
  3: required i64 entityId,
  4: required i64 parentId,
  5: optional i64 pageNum,
  6: optional i64 pageSize,
  255: optional base.Base Base
}

struct EntityCommentListResp {
  1: required list<Comment> commentList,
  2: required i64 total,
  255: optional base.BaseResp BaseResp
}

struct UserCommentListReq {
  1: required i64 bizId,
  2: required i64 entityTypeId,
  3: required i64 uid,
  4: optional i64 pageNum,
  5: optional i64 pageSize,
  255: optional base.Base Base
}

struct UserCommentListResp {
  1: required list<Comment> commentList,
  2: required i64 total,
  255: optional base.BaseResp BaseResp
}

service CommentService {
  CreateCommentEntityResp createCommentEntity(1: CreateCommentEntityReq req)
  AllCommentEntityListResp allCommentEntityList(1: AllCommentEntityListReq req)
  PublishCommentResp publishComment(1: PublishCommentReq req)
  DeleteCommentResp deleteComment(1: DeleteCommentReq req)
  GetCommentByIdResp getCommentById(1: GetCommentByIdReq req)
  EntityCommentListResp entityCommentList(1: EntityCommentListReq req)
  UserCommentListResp userCommentList(1: UserCommentListReq req)
}