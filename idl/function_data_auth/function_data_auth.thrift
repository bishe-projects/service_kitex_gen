namespace go function_data_auth
include '../base/base.thrift'

struct FunctionDataAuth {
  1: i64 id,
  2: i64 uid,
  3: i64 functionId,
  4: string functionName,
  5: i64 businessId,
  6: string businessName,
  7: i64 authorizerId
}

struct CreateAuthReq {
  1: required i64 uid,
  2: required i64 functionId,
  3: required i64 businessId,
  255: optional base.Base Base
}

struct CreateAuthResp {
  255: optional base.BaseResp BaseResp
}

struct GetAuthListReq {
  1: required i64 functionId,
  2: optional i64 uid,
  3: optional i64 businessId,
  255: optional base.Base Base
}

struct GetAuthListResp {
  1: required list<FunctionDataAuth> authList,
  255: optional base.BaseResp BaseResp
}

struct HasAuthReq {
  1: required i64 uid,
  2: required i64 functionId,
  3: required i64 businessId,
  255: optional base.Base Base
}

struct HasAuthResp {
  1: required bool has,
  255: optional base.BaseResp BaseResp
}

service FunctionDataAuthService {
  CreateAuthResp createAuth(1: CreateAuthReq req)
  GetAuthListResp getAuthList(1: GetAuthListReq req)
  HasAuthResp hasAuth(1: HasAuthReq req)
}