namespace go demo_user
include '../base/base.thrift'
include '../comment/comment.thrift'
include '../demo_commodity/commodity.thrift'

struct User {
    1: i64 uid,
    2: string username
}

struct UserLoginReq {
    1: required string username,
    2: required string password,
    255: optional base.Base Base
}

struct UserLoginResp {
    1: optional User user,
    3: optional string token,
    255: optional base.BaseResp BaseResp
}

struct UserRegisterReq {
    1: required string username,
    2: required string password,
    3: required string confirmPassword,
    255: optional base.Base Base
}

struct UserRegisterResp {
    255: optional base.BaseResp BaseResp
}

struct GetUserByIDReq {
    1: required i64 uid,
    255: optional base.Base Base
}

struct GetUserByIDResp {
    1: optional User user,
    255: optional base.BaseResp BaseResp
}

struct GetUserByUsernameReq {
    1: required string username,
    255: optional base.Base Base
}

struct GetUserByUsernameResp {
    1: optional User user,
    255: optional base.BaseResp BaseResp
}

struct GetUserListByIdsReq {
    1: required list<i64> ids,
    255: optional base.Base Base
}

struct GetUserListByIdsResp {
    1: required list<User> userList,
    255: optional base.BaseResp BaseResp
}

struct OrderComment {
    1: comment.Comment comment,
    2: commodity.Commodity commodity
}

struct GetUserCommentListReq {
    1: required i64 uid,
    255: optional base.Base Base
}

struct GetUserCommentListResp {
    1: required list<OrderComment> commentList,
    255: optional base.BaseResp BaseResp
}

service UserService {
    UserLoginResp userLogin(1: UserLoginReq req)
    UserRegisterResp userRegister(1: UserRegisterReq req)
    GetUserByIDResp getUserByID(1: GetUserByIDReq req)
    GetUserByUsernameResp getUserByUsername(1: GetUserByUsernameReq req)
    GetUserListByIdsResp getUserListByIds(1: GetUserListByIdsReq req)
    GetUserCommentListResp getUserCommentList(1: GetUserCommentListReq req)
}