namespace go user
include '../base/base.thrift'

struct Role {
    1: i64 id,
    2: string name,
    3: optional i64 manageFunctionId,
    4: optional string manageFunctionName
}

struct User {
    1: i64 uid,
    2: string username,
    3: list<Role> roles
}

struct UserLoginReq {
    1: required string username,
    2: required string password,
    255: optional base.Base Base
}

struct UserLoginResp {
    1: optional User user,
    3: optional string token,
    255: optional base.BaseResp BaseResp
}

struct UserRegisterReq {
    1: required string username,
    2: required string password,
    3: required string confirmPassword,
    255: optional base.Base Base
}

struct UserRegisterResp {
    255: optional base.BaseResp BaseResp
}

struct GetUserByIDReq {
    1: required i64 uid,
    255: optional base.Base Base
}

struct GetUserByIDResp {
    1: optional User user,
    255: optional base.BaseResp BaseResp
}

struct GetUserByUsernameReq {
    1: required string username,
    255: optional base.Base Base
}

struct GetUserByUsernameResp {
    1: optional User user,
    255: optional base.BaseResp BaseResp
}

struct ChangePasswordReq {
    1: required string password,
    2: required string newPassword,
    3: required string confirmPassword,
    255: optional base.Base Base
}

struct ChangePasswordResp {
    255: optional base.BaseResp BaseResp
}

struct FunctionAdminListReq {
    1: required i64 functionId,
    255: optional base.Base Base
}

struct FunctionAdminListResp {
    1: required list<User> adminList,
    255: optional base.BaseResp BaseResp
}

struct AllRoleListReq {
    255: optional base.Base Base
}

struct AllRoleListResp {
    1: required list<Role> roleList,
    255: optional base.BaseResp BaseResp
}

struct UserRoleListReq {
    1: required i64 uid,
    255: optional base.Base Base
}

struct UserRoleListResp {
    1: required list<Role> roleList,
    255: optional base.BaseResp BaseResp
}

struct UserAddRoleReq {
    1: required i64 uid,
    2: required i64 roleId,
    3: required i64 functionId,
    255: optional base.Base Base
}

struct UserAddRoleResp {
    255: optional base.BaseResp BaseResp
}

struct UserRemoveRoleReq {
    1: required i64 uid,
    2: required i64 roleId,
    3: required i64 functionId,
    255: optional base.Base Base
}

struct UserRemoveRoleResp {
    255: optional base.BaseResp BaseResp
}

struct HasUserRoleReq {
    1: required i64 uid,
    2: required i64 roleId,
    3: optional i64 functionId,
    255: optional base.Base Base
}

struct HasUserRoleResp {
    1: required bool has,
    255: optional base.BaseResp BaseResp
}

service UserService {
    UserLoginResp userLogin(1: UserLoginReq req)
    UserRegisterResp userRegister(1: UserRegisterReq req)
    GetUserByIDResp getUserByID(1: GetUserByIDReq req)
    GetUserByUsernameResp getUserByUsername(1: GetUserByUsernameReq req)
    ChangePasswordResp changePassword(1: ChangePasswordReq req)
    FunctionAdminListResp functionAdminList(1: FunctionAdminListReq req)
    AllRoleListResp allRoleList(1: AllRoleListReq req)
    UserRoleListResp userRoleList(1: UserRoleListReq req)
    UserAddRoleResp userAddRole(1: UserAddRoleReq req)
    UserRemoveRoleResp userRemoveRole(1: UserRemoveRoleReq req)
    HasUserRoleResp hasUserRole(1: HasUserRoleReq req)
}