namespace go rank
include '../base/base.thrift'

enum SameScoreSortType {
    // 先到的在前面
    FIRST = 0,
    // 后到的在前面
    LAST = 1
}

struct RankConfig {
    // key
    1: required string key,
    // 业务线id
    2: required i64 businessId,
    // 榜单展示长度
    3: required i64 limit,
    // 分数从高到低 or 从低到高
    4: required bool isBigTop,
}

struct RankItem {
    1: required string member,
    2: required double score
}

struct RankList {
    1: required string key,
    2: required string name,
    3: required i64 businessId,
    4: required list<RankItem> rankItems
}

struct GetRankConfigsReq {
    1: required i64 businessId,
    255: optional base.Base Base
}

struct GetRankConfigsResp {
    1: required list<string> rankConfigs,
    255: optional base.BaseResp BaseResp
}

struct CreateRankConfigReq {
    1: required RankConfig config,
    255: optional base.Base Base
}

struct CreateRankConfigResp {
    255: optional base.BaseResp BaseResp
}

struct DeleteRankConfigReq {
    1: required string key,
    2: required i64 businessId,
    255: optional base.Base Base
}

struct DeleteRankConfigResp {
    255: optional base.BaseResp BaseResp
}

struct GetRankConfigReq {
    1: required string key,
    2: required i64 businessId,
    255: optional base.Base Base
}

struct GetRankConfigResp {
    1: optional RankConfig rankConfig,
    255: optional base.BaseResp BaseResp
}

struct GetRankListsReq {
    1: required string key,
    2: required i64 businessId,
    255: optional base.Base Base
}

struct GetRankListsResp {
    1: list<string> rankLists,
    255: optional base.BaseResp BaseResp
}

struct CreateRankListReq {
    1: required string key,
    2: required i64 businessId,
    3: required string name,
    255: optional base.Base Base
}

struct CreateRankListResp {
    255: optional base.BaseResp BaseResp
}

struct UpdateRankListReq {
    1: required string key,
    2: required i64 businessId,
    3: required string name,
    4: required string member,
    5: required double delta,
    255: optional base.Base Base
}

struct UpdateRankListResp {
    255: optional base.BaseResp BaseResp
}

struct DeleteRankListReq {
    1: required string key,
    2: required i64 businessId,
    3: required string name,
    255: optional base.Base Base
}

struct DeleteRankListResp {
    255: optional base.BaseResp BaseResp
}

struct GetRankListReq {
    1: required string key,
    2: required i64 businessId,
    3: required string name,
    4: required i64 start,
    5: required i64 length,
    255: optional base.Base Base
}

struct GetRankListResp {
    1: required RankList rankList,
    255: optional base.BaseResp BaseResp
}

struct RemoveRankListMemberReq {
    1: required string key,
    2: required i64 businessId,
    3: required string name,
    4: required string member,
    255: optional base.Base Base
}

struct RemoveRankListMemberResp {
    255: optional base.BaseResp BaseResp
}

service RankService {
    GetRankConfigsResp getRankConfigs(1: GetRankConfigsReq req)
    CreateRankConfigResp createRankConfig(1: CreateRankConfigReq req)
    DeleteRankConfigResp deleteRankConfig(1: DeleteRankConfigReq req)
    GetRankConfigResp getRankConfig(1: GetRankConfigReq req)
    GetRankListsResp getRankLists(1: GetRankListsReq req)
    CreateRankListResp createRankList(1: CreateRankListReq req)
    UpdateRankListResp updateRankList(1: UpdateRankListReq req)
    DeleteRankListResp deleteRankList(1: DeleteRankListReq req)
    GetRankListResp getRankList(1: GetRankListReq req)
    RemoveRankListMemberResp removeRankListMember(1: RemoveRankListMemberReq req)
}