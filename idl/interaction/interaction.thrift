namespace go interaction
include '../base/base.thrift'

struct InteractiveEntity {
  1: required i64 id,
  2: required i64 bizId,
  3: required string name,
  4: required string desc
}

struct InteractiveAction {
  1: required i64 id,
  2: required i64 bizId,
  3: required string name,
  4: required string desc
}

struct Interaction {
  1: required i64 bizId,
  2: required i64 actionId,
  3: required i64 initiatorEntityId,
  4: required i64 initiatorId,
  5: required i64 acceptorEntityId,
  6: required i64 acceptorId
}

struct CreateEntityReq {
  1: required i64 bizId,
  2: required string name,
  3: required string desc,
  255: optional base.Base Base
}

struct CreateEntityResp {
  1: optional InteractiveEntity entity,
  255: optional base.BaseResp BaseResp
}

struct AllEntityListReq {
  1: optional i64 bizId,
  255: optional base.Base Base
}

struct AllEntityListResp {
  1: required list<InteractiveEntity> entityList,
  255: optional base.BaseResp BaseResp
}

struct CreateActionReq {
  1: required i64 bizId,
  2: required string name,
  3: required string desc,
  255: optional base.Base Base
}

struct CreateActionResp {
  1: optional InteractiveAction action,
  255: optional base.BaseResp BaseResp
}

struct AllActionListReq {
  1: optional i64 bizId,
  255: optional base.Base Base
}

struct AllActionListResp {
  1: required list<InteractiveAction> actionList,
  255: optional base.BaseResp BaseResp
}

struct CreateInteractionReq {
  1: required Interaction interaction,
  255: optional base.Base Base
}

struct CreateInteractionResp {
  255: optional base.BaseResp BaseResp
}

struct RemoveInteractionReq {
  1: required Interaction interaction,
  255: optional base.Base Base
}

struct RemoveInteractionResp {
  255: optional base.BaseResp BaseResp
}

struct GetInteractionListReq {
  1: required i64 bizId,
  2: required i64 actionId,
  3: required i64 initiatorEntityId,
  4: required i64 acceptorEntityId,
  5: optional i64 initiatorId,
  6: optional i64 acceptorId,
  255: optional base.Base Base
}

struct GetInteractionListResp {
  1: required list<Interaction> interactionList,
  255: optional base.BaseResp BaseResp
}

service InteractionService {
  CreateEntityResp createEntity(1: CreateEntityReq req)
  AllEntityListResp allEntityList(1: AllEntityListReq req)
  CreateActionResp createAction(1: CreateActionReq req)
  AllActionListResp allActionList(1: AllActionListReq req)
  CreateInteractionResp createInteraction(1: CreateInteractionReq req)
  RemoveInteractionResp removeInteraction(1: RemoveInteractionReq req)
  GetInteractionListResp getInteractionList(1: GetInteractionListReq req)
}