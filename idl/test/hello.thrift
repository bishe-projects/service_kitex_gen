namespace go test.hello
include '../base/base.thrift'

struct Request {
    1: string message
}

struct Response {
    1: string message
}

struct AddRequest {
    1: i64 first,
    2: i64 second,
    3: i64 third,
    255: optional base.Base Base
}

struct AddResponse {
    1: i64 sum,
    255: optional base.BaseResp BaseResp
}

service Hello {
    Response echo(1: Request req)
    AddResponse add(1: AddRequest req)
}
